﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using town360_core_bff.models;

namespace town360_core_bff.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class MenuController : ControllerBase
    {
        private IEnumerable<Menu> menuItems = new[]
        {
            new Menu("Home", "home"), new Menu("Test", "test")
        };

        private readonly ILogger<MenuController> _logger;

        public MenuController(ILogger<MenuController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public IEnumerable<Menu> Get()
        {
            return menuItems.ToArray(); 
        }

    }
}