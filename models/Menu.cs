﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace town360_core_bff.models
{
    public class Menu
    {
        public Menu(string dName, string rName)
        {
            this.displayName = dName;
            this.routeName = rName;
        }

        public string displayName { get; set; }
        public string routeName { get; set; }
    }
}
